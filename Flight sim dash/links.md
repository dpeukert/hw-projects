- https://www.propwashsim.com/store/dual-encoder-kit
- https://ts.thrustmaster.com/download/accessories/manuals/T16000M/T16000M-FCS-cockpit-Setup.pdf
- https://www.thingiverse.com/thing:4730037
- https://www.thingiverse.com/thing:4654227

# HID display
- https://web.archive.org/web/20150813073104/http://www.circuitgizmos.com/products/cgu431/cgu431.shtml
- https://www.usb.org/sites/default/files/hutrr29b_0.pdf
- https://usb.org/sites/default/files/hut1_22.pdf
- https://www.sparkfun.com/products/15795
- https://www.sparkfun.com/products/18288
- https://hackaday.com/2012/10/23/driving-an-lcd-character-display-using-custom-hid-codes/#comment-833738
- https://www.sparkfun.com/tutorials/337
