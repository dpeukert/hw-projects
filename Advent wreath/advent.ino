#include <DS1302RTC.h>
#include <Time.h>

DS1302RTC RTC(12, 11, 10);
time_t sundays[4];

void setup()
{
  digitalWrite(9, LOW); // GND
  pinMode(9, OUTPUT);
  digitalWrite(8, HIGH); // VCC
  pinMode(8, OUTPUT);
  
  digitalWrite(2, HIGH);
  pinMode(2, OUTPUT);
  digitalWrite(3, HIGH);
  pinMode(3, OUTPUT);
  digitalWrite(4, HIGH);
  pinMode(4, OUTPUT);
  digitalWrite(5, HIGH);
  pinMode(5, OUTPUT);

  digitalWrite(13, LOW);
  pinMode(13, OUTPUT);
  
  Serial.begin(9600);
  setSyncProvider(RTC.get);

  tmElements_t christmastime;
  christmastime.Second = 0; 
  christmastime.Hour = 0;
  christmastime.Minute = 0;
  christmastime.Day = 24;
  christmastime.Month = 12;
  christmastime.Year = year() - 1970;
  time_t christmas = makeTime(christmastime);
  sundays[3] = christmas - ((weekday(christmas) - 1) * 86400);
  sundays[2] = sundays[3] - 604800; // 86400 * 7
  sundays[1] = sundays[3] - 1209600; // 86400 * 8 * 2
  sundays[0] = sundays[3] - 1814400; // 86400 * 8 * 3

  Serial.println(String(dayStr(weekday(christmas))));
  Serial.println(String(day(christmas))+"/"+String(month(christmas))+"/"+String(year(christmas)));
  Serial.println("");
  Serial.println(String(dayStr(weekday(sundays[3]))));
  Serial.println(String(day(sundays[3]))+"/"+String(month(sundays[3]))+"/"+String(year(sundays[3])));
  Serial.println("");
  Serial.println(String(dayStr(weekday(sundays[2]))));
  Serial.println(String(day(sundays[2]))+"/"+String(month(sundays[2]))+"/"+String(year(sundays[2])));
  Serial.println("");
  Serial.println(String(dayStr(weekday(sundays[1]))));
  Serial.println(String(day(sundays[1]))+"/"+String(month(sundays[1]))+"/"+String(year(sundays[1])));
  Serial.println("");
  Serial.println(String(dayStr(weekday(sundays[0]))));
  Serial.println(String(day(sundays[0]))+"/"+String(month(sundays[0]))+"/"+String(year(sundays[0])));
}

void loop()
{
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  time_t nowtime = now();

  Serial.println(String(dayStr(weekday(nowtime))));
  Serial.println(String(day(nowtime))+"/"+String(month(nowtime))+"/"+String(year(nowtime))+" "+String(hour(nowtime))+":"+String(minute(nowtime)));
  
  
  for(int sunday=0; sunday < 4; sunday++)
  {
    if(nowtime >= sundays[sunday])
    {
      digitalWrite(sunday+2, HIGH);
    }
    else
    {
      digitalWrite(sunday+2, LOW);
    }
  }
  delay(30000);
}
