# My personal hardware projects

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`) with the following exceptions:
* The [Tone](https://github.com/bhagman/Tone) Arduino library by [Brett Hagman](https://roguerobotics.com/) licensed under the [MIT license](E36Dash/Arduino/libraries/Tone/license.txt)
  * `E36Dash/Arduino/libraries/Tone/`
