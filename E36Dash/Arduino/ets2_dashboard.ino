#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

int lcd_key     = 0;
int adc_key_in  = 0;
int last_key    = 1500; // must be bigger than 1000

#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

int read_LCD_buttons() {
  adc_key_in = analogRead(0);

  if (adc_key_in > 1000) return btnNONE;

  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 250)  return btnUP;
  if (adc_key_in < 450)  return btnDOWN;
  if (adc_key_in < 650)  return btnLEFT;
  if (adc_key_in < 850)  return btnSELECT;

  return btnNONE;
}

void setup()
{
  lcd.clear();
  Serial.begin(115200);
}

void loop()
{
  lcd_key = read_LCD_buttons();
  switch (lcd_key) {
    case btnLEFT:
      {
        if (last_key > 1000)
        {
          Serial.println("left");
        }
        break;
      }
    case btnRIGHT:
      {
        if (last_key > 1000)
        {
          Serial.println("right");
        }
        break;
      }
    case btnUP:
      {
        if (last_key > 1000)
        {
          Serial.println("up");
        }
        break;
      }
    case btnDOWN:
      {
        if (last_key > 1000)
        {
          Serial.println("down");
        }
        break;
      }
    case btnSELECT:
      {
        if (last_key > 1000)
        {
          Serial.println("select");
        }
        break;
      }
    case btnNONE:
      {
        if (last_key < 1001)
        {
          Serial.println("none");
        }
        break;
      }
  }
  last_key = adc_key_in;
  delay(50);
}
