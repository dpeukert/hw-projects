#include <limits.h>
#include <Tone.h>

#define DEBUG				1

#define MAGIC_NUMBER		42

#define SPEED_TIMER			3
#define RPM_TIMER			1

#define FUEL_PIN			46		// X271/1		corresponds to timer 5
#define SPEED_PIN			5		// X271/4
#define RPM_PIN				11		// X16/20
#define TEMP_PIN			6		// X17/18		corresponds to timer 4
#define BAT_PIN				999		// X17/5
#define BEAMS_PIN			999		// X16/1
#define OIL_PIN				999		// X17/19
#define L_INDICATOR_PIN		999		// X16/4
#define R_INDICATOR_PIN		999		// X16/3
#define ASCT_PIN			999		// X17/12
#define FOG_FRONT_PIN		999		// X17/8
#define FOG_REAR_PIN		999		// ?
#define DPF_PIN				999		// ?			not	sure if it's really DPF
#define ROLLOVER_PIN		999		// X16/10
#define CHECK_ENGINE_PIN	999		// X16/16
#define INDICATORS_PIN		999		// ?
#define AUTOMATIC_PIN		999		// X16/5
#define CHECK_CONTROL_PIN	999		// X17/26
#define SEAT_BELTS_PIN		999		// X17/14
#define ABS_PIN				999		// X17/21
#define BRAKE_PADS_PIN		999		// X16/19
#define BRAKE_FLUID_PIN		999		// X16/18
#define PARKING_BRAKE_PIN	999		// X16/17
#define PANINI_MAKER_PIN	999		// ?			no idea what this is
#define AIRBAG_PIN			999		// X16/14

Tone SpeedTone;
Tone RPMTone;

void setup() {
	Serial.begin(115200);
	Serial.setTimeout(0);

	Serial1.begin(115200);

	pinMode(FUEL_PIN, OUTPUT);
	SpeedTone.begin(SPEED_PIN, SPEED_TIMER);
	RPMTone.begin(RPM_PIN, RPM_TIMER);
	pinMode(TEMP_PIN, OUTPUT);

	// pinMode(BAT_PIN, OUTPUT);
	// pinMode(BEAMS_PIN, OUTPUT);
	// pinMode(OIL_PIN, OUTPUT);
	// pinMode(L_INDICATOR_PIN, OUTPUT);
	// pinMode(R_INDICATOR_PIN, OUTPUT);
	// pinMode(ASCT_PIN, OUTPUT);
	// pinMode(FOG_FRONT_PIN, OUTPUT);
	// pinMode(FOG_REAR_PIN, OUTPUT);
	// pinMode(DPF_PIN, OUTPUT);
	// pinMode(ROLLOVER_PIN, OUTPUT);
	// pinMode(CHECK_ENGINE_PIN, OUTPUT);
	// pinMode(INDICATORS_PIN, OUTPUT);
	// pinMode(AUTOMATIC_PIN, OUTPUT);
	// pinMode(CHECK_CONTROL_PIN, OUTPUT);
	// pinMode(SEAT_BELTS_PIN, OUTPUT);
	// pinMode(ABS_PIN, OUTPUT);
	// pinMode(BRAKE_PADS_PIN, OUTPUT);
	// pinMode(BRAKE_FLUID_PIN, OUTPUT);
	// pinMode(PARKING_BRAKE_PIN, OUTPUT);
	// pinMode(PANINI_MAKER_PIN, OUTPUT);
	// pinMode(AIRBAG_PIN, OUTPUT);
}

void loop() {
	if(Serial.available() > 0) {
		#ifdef DEBUG
			Serial1.println('\n');
			Serial1.println(millis());
			Serial1.println(Serial.available());
			Serial1.println("Waiting for magic number");
		#endif

		bool delimiterFound = false;

		while(delimiterFound == false) {
			if(Serial.read() == MAGIC_NUMBER) {
				delimiterFound = true;
			}
		}

		#ifdef DEBUG
			Serial1.println("Waiting for rest of data");
		#endif

		while(Serial.available() < 4) {}

		#ifdef DEBUG
			Serial1.println("Waiting for parsing");
		#endif

		parseInput();
	}
}

void parseInput() {
	// Can we continue or did something get lost?
	if(isNextByteMagic() == true) {
		#ifdef DEBUG
			Serial1.println("Output section got lost, skipping packet");
		#endif

		return;
	}

	// Get the output we want to work with - 1 byte
	byte output = Serial.read();

	if(output == -1) {
		#ifdef DEBUG
			Serial1.println("Output section is invalid, skipping packet");
		#endif

		return;
	}

	// Get the value - 3 bytes
	long value = 0;
	byte value_byte;

	for(int i = 0; i < 3; i++) {
		// Can we continue or did something get lost?
		if(isNextByteMagic() == true) {
			#ifdef DEBUG
				Serial1.println("Part of value section got lost, skipping packet");
			#endif

			return;
		}

		value_byte = Serial.read();

		if(value_byte == -1) {
			#ifdef DEBUG
				Serial1.println("Part of value section is invalid, skipping packet");
			#endif

			return;
		}

		value += (long)value_byte << (8*(2-i));
	}

	// Extend the MSB to the last unused byte
	if(value & 8388608) { // Check if the 24th bit is 1, if it's not, everything is poggers as it is
		// First 24 bits are 0 => original bits of the value are kept
		// Last 8 bits are 1 => bits of the value are set to 1
		value |= ~(((long)1 << 24) - 1);
	}

	// Serial1.println(output, DEC);
	// Serial1.println(value, DEC);

	setOutput(output, value);
}

void setOutput(byte output, long value) {
	byte type;
	byte pin;
	Tone * tone_instance;
	unsigned long max_value;

	// Prepare data for output

	switch(output) {
		case 101: // Fuel gauge
			type = 1;
			pin = FUEL_PIN;
			value = map(value, 0, 10000, 0, 153); // TODO: find PWM values for 0 and 100
			max_value = 153;					  // TODO: find min and max PWM values
			break;

		case 102: // Speed gauge
			type = 2;
			tone_instance = &SpeedTone;
			value = map(value, 3000, 25000, 35, 319);
			if(value < 0) {
				value = value * -1;
			}
			if(value <= 13) {
				value = 0;
			}
			max_value = 391;
			break;

		case 103: // RPM gauge
			type = 2;
			tone_instance = &RPMTone;
			if (value >= 0) { // when the value is positive, use it as an absolute one
				value = map(value, 100000, 700000, 37, 228);
			} else { // when negative, take the absolute value and use it as a percentage
				value = map(value * -1, 1429, 10000, 37, 228);
			}
			if(value <= 11) {
				// use 5 as the minimal value, as that's the point where the temp gauge kicks in
				value = 5;
			}
			max_value = 317;
			break;

		case 104: // Temp gauge
			type = 1;
			pin = TEMP_PIN;
			value = map(value, 5500, 12100, 122, 0);
			max_value = 122;
			break;

		// case 105: // Battery
		// 	type = 3;
		// 	pin = BAT_PIN;
		// 	break;

		// case 106: // High beams
		// 	type = 3;
		// 	pin = BEAMS_PIN;
		// 	break;

		// case 107: // Oil pressure
		// 	type = 3;
		// 	pin = OIL_PIN;
		// 	break;

		// case 108: // Left indicator
		// 	type = 3;
		// 	pin = L_INDICATOR_PIN;
		// 	break;

		// case 109: // Right indicator
		// 	type = 3;
		// 	pin = R_INDICATOR_PIN;
		// 	break;

		// case 110: // ASC+T
		// 	type = 3;
		// 	pin = ASCT_PIN;
		// 	break;

		// case 111: // Front foglamps
		// 	type = 3;
		// 	pin = FOG_FRONT_PIN;
		// 	break;

		// case 112: // Rear foglamps
		// 	type = 3;
		// 	pin = FOG_REAR_PIN;
		// 	break;

		// case 113: // Diesel Particulate Filter
		// 	type = 3;
		// 	pin = DPF_PIN;
		// 	break;

		// case 114: // Rollover
		// 	type = 3;
		// 	pin = ROLLOVER_PIN;
		// 	break;

		// case 115: // Check engine
		// 	type = 3;
		// 	pin = CHECK_ENGINE_PIN;
		// 	break;

		// case 116: // Indicators
		// 	type = 3;
		// 	pin = INDICATORS_PIN;
		// 	break;

		// case 117: // Automatic transmission
		// 	type = 3;
		// 	pin = AUTOMATIC_PIN;
		// 	break;

		// case 118: // Check control
		// 	type = 3;
		// 	pin = CHECK_CONTROL_PIN;
		// 	break;

		// case 119: // Seat belts
		// 	type = 3;
		// 	pin = SEAT_BELTS_PIN;
		// 	break;

		// case 120: // ABS
		// 	type = 3;
		// 	pin = ABS_PIN;
		// 	break;

		// case 121: // Brake pads
		// 	type = 3;
		// 	pin = BRAKE_PADS_PIN;
		// 	break;

		// case 122: // Brake fluid
		// 	type = 3;
		// 	pin = BRAKE_FLUID_PIN;
		// 	break;

		// case 123: // Parking brake
		// 	type = 3;
		// 	pin = PARKING_BRAKE_PIN;
		// 	break;

		// case 124: // Panini maker
		// 	type = 3;
		// 	pin = PANINI_MAKER_PIN;
		// 	break;

		// case 125: // Airbag
		// 	type = 3;
		// 	pin = AIRBAG_PIN;
		// 	break;

		default:
			return;
	}

	// Output data

	switch(type) {
		case 1:
			value = constrain(value, 0, max_value);
			analogWrite(pin, value);

			#ifdef DEBUG
				Serial1.println("Changing PWM output " + String(output) + " at pin " + String(pin) + " to " + String(value));
			#endif

			break;

		case 2:
			value = constrain(value, 0, max_value);
			if(value > 0) {
				(*tone_instance).play(value);

				#ifdef DEBUG
					Serial1.println("Changing tone output " + String(output) + " to " + String(value));
				#endif
			} else {
				(*tone_instance).stop();

				#ifdef DEBUG
					Serial1.println("Stopping tone output " + String(output));
				#endif
			}
			break;

		case 3:
			if(value != 0) {
				digitalWrite(pin, HIGH);

				#ifdef DEBUG
					Serial1.println("Switching on output " + String(output) + " at pin " + String(pin));
				#endif
			} else {
				digitalWrite(pin, LOW);

				#ifdef DEBUG
					Serial1.println("Switching off output " + String(output) + " at pin " + String(pin));
				#endif
			}
			break;

		default:
			return;
	}
}

bool isNextByteMagic() {
	if(Serial.peek() == MAGIC_NUMBER) {
		return true;
	} else {
		return false;
	}
}