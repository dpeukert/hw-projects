# BUSE BS 120

## Description
- a driver board receives data using the IBIS protocol
- 3 display boards, each display module contains X LED modules, each LED module contains Y LEDs

## Links
- http://www.forum.omsi.cz/viewtopic.php?f=6&t=10480

# JKZ VSII
- a driver board receives data using the IBIS protocol
  - 24 V input
  - [original wiring](jkz_vsii_diagram_original_ibis_wiring.jpg)
  - starts smelling and smoking after a while (probably shouldn't be doing that)
- 3 display boards, each display module contains 7 LED modules, each LED module contains 5x8 LEDs
  - each display board is connected using a 20 wire ribbon cable with a FC-20P connector
  - the columns are driven by a STM M5451 LED driver on each display board
    - bit number === column driver, all 35 bits are used
    - the position of the module is set by bridging one of the J1-J4 jumpers, this causes the M5451 to use on of the four Data in lines
  - the rows are driven directly by the driver board
    - all corresponding row pins on every LED module (Para Light C3580SR) on every display board are bridged together and behave as a single row
    - transistors for each row on both driver and display boards, not sure what
  - driven using column or row multiplexing

## Ribbon cable pinout - [diagram](jkz_vsii_diagram.png)

| Pin number | Pin usage                                |
| ---------: | :--------------------------------------- |
|          1 | bottom of D2 on the driver board (Vrow?) |
|          2 | 0 V                                      |
|          3 | Vdriver                                  |
|          4 | Data in - J4                             |
|          5 | Data in - J2                             |
|          6 | Clock in                                 |
|          7 | T1 (on the driver board)                 |
|          8 | T3 (on the driver board)                 |
|          9 | T5 (on the driver board)                 |
|         10 | T7 (on the driver board)                 |
|         11 | T8 (on the driver board)                 |
|         12 | T6 (on the driver board)                 |
|         13 | T4 (on the driver board)                 |
|         14 | T2 (on the driver board)                 |
|         15 | Vdriver                                  |
|         16 | Data in - J1                             |
|         17 | Data in - J3                             |
|         18 | 0 V                                      |
|         19 | 0 V                                      |
|         20 | bottom of D2 on the driver board (Vrow?) |

# IBIS protocol links
- http://jakub-svec.eu/stranky.php?stranky=odbs
- http://www.ebastlirna.cz/modules.php?name=Forums&file=viewtopic&t=85672
- https://www.railnet.sk/view.php?cisloclanku=2014010002
- https://oi.fd.cvut.cz/prace/F6-BP-2019-Eichler-Adam-Bakalarka_sm.pdf
- https://winibis.de/
- https://xatlabs.com/de/produkte/lynx/
- https://github.com/Seitzruh/IBIS-Test/blob/master/app/src/main/res/values/arrays.xml
- https://www.hwpro.cz/oc/index.php?route=product/product&product_id=79
- https://github.com/Mezgrman/pyFIS
- https://www.vdv.de/telegrammvergleichstabelle-ibis-zu-ibis-ip-v1.0.pdfx
